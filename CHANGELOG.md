# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].

[comment]: <> (Added:      new features)
[comment]: <> (Changed:    changes in existing functionality)
[comment]: <> (Deprecated: soon-to-be removed features)
[comment]: <> (Removed:    now removed features)
[comment]: <> (Fixed:      any bug fixes)
[comment]: <> (Security:   in case of vulnerabilities)



## [1.1.1] - 2023-07-28

### Fixed
* Minor documentation fixes.



## [1.1.0] - 2023-07-04

### Added
* Support generating JSON schemas via [`schemars`](https://graham.cool/schemars), enabled through the new feature `json-schema`.



## [1.0.0] - 2023-07-02

### Added
* Initial implementation.



[Keep a Changelog]: https://keepachangelog.com/en/1.1.0
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[1.1.1]: https://gitlab.com/Fraser999/serde-map-to-array/-/compare/v1.0.0...v1.1.1
[1.1.0]: https://gitlab.com/Fraser999/serde-map-to-array/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/Fraser999/serde-map-to-array/-/releases/v1.0.0
